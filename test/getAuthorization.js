var oauth= require('oauth');
var CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
var timeCorrectionMsec = (timestamp * 1000) - (new Date()).getTime()
function timestamp() {
  var t = (new Date()).getTime();
  return Math.floor(t / 1000);
}
function nonce(length) {
    var chars = CHARS;
    var result = "";
    for (var i = 0; i < length; ++i) {
        var rnum = Math.floor(Math.random() * chars.length);
        result += chars.substring(rnum, rnum+1);
    }
    return result;
}
function percentEncode (s) {
  if (s == null) {
      return "";
  }
  if (s instanceof Array) {
      var e = "";
      for (var i = 0; i < s.length; ++s) {
          if (e != "") e += '&';
          e += percentEncode(s[i]);
      }
      return e;
  }
  s = encodeURIComponent(s);
  // Now replace the values which encodeURIComponent doesn't do
  // encodeURIComponent ignores: - _ . ! ~ * ' ( )
  // OAuth dictates the only ones you can ignore are: - _ . ~
  // Source: http://developer.mozilla.org/en/docs/Core_JavaScript_1.5_Reference:Global_Functions:encodeURIComponent
  s = s.replace(/\!/g, "%21");
  s = s.replace(/\*/g, "%2A");
  s = s.replace(/\'/g, "%27");
  s = s.replace(/\(/g, "%28");
  s = s.replace(/\)/g, "%29");
  return s;
}
function decodePercent (s) {
  if (s != null) {
      // Handle application/x-www-form-urlencoded, which is defined by
      // http://www.w3.org/TR/html4/interact/forms.html#h-17.13.4.1
      s = s.replace(/\+/g, " ");
  }
  return decodeURIComponent(s);
}
function decodeForm (form) {
  var list = [];
  var nvps = form.split('&');
  for (var n = 0; n < nvps.length; ++n) {
      var nvp = nvps[n];
      if (nvp == "") {
          continue;
      }
      var equals = nvp.indexOf('=');
      var name;
      var value;
      if (equals < 0) {
          name = decodePercent(nvp);
          value = null;
      } else {
          name = decodePercent(nvp.substring(0, equals));
          value = decodePercent(nvp.substring(equals + 1));
      }
      list.push([name, value]);
  }
  return list;
}
function getParameterList (parameters) {
  if (parameters == null) {
      return [];
  }
  if (typeof parameters != "object") {
      return decodeForm(parameters + "");
  }
  if (parameters instanceof Array) {
      return parameters;
  }
  var list = [];
  for (var p in parameters) {
      list.push([p, parameters[p]]);
  }
  return list;
}

function getParameters () {
  return [
    ["oauth_version", "1.0"],
    ["oauth_consumer_key", '1OzkVwpCE8_jwNFxoy6ETA'],
    ["oauth_timestamp", timestamp()],
    ["oauth_nonce", nonce(6)],
    ["oauth_signature_method", 'HMAC-SHA1'],
    ["oauth_token", "p2PFLuAgvPH30OahLTvkZg"]
  ];
}

function getAuthorizationHeader(realm, parameters) {
   var header = 'OAuth realm="' + percentEncode(realm) + '"';
   var list = getParameterList(parameters);
   for (var p = 0; p < list.length; ++p) {
       var parameter = list[p];
       var name = parameter[0];
       if (name.indexOf("oauth_") == 0) {
           header += ',' + percentEncode(name) + '="' + percentEncode(parameter[1]) + '"';
       }
   }
   return header;
}

function parseUri (str) {
    /* This function was adapted from parseUri 1.2.1
       http://stevenlevithan.com/demo/parseuri/js/assets/parseuri.js
     */
    var o = {key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
             parser: {strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@\/]*):?([^:@\/]*))?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/ }};
    var m = o.parser.strict.exec(str);
    var uri = {};
    var i = 14;
    while (i--) uri[o.key[i]] = m[i] || "";
    return uri;
}

function normalizeUrl(url) {
    var uri = parseUri(url);
    var scheme = uri.protocol.toLowerCase();
    var authority = uri.authority.toLowerCase();
    var dropPort = (scheme == "http" && uri.port == 80)
                || (scheme == "https" && uri.port == 443);
    if (dropPort) {
        // find the last : in the authority
        var index = authority.lastIndexOf(":");
        if (index >= 0) {
            authority = authority.substring(0, index);
        }
    }
    var path = uri.path;
    if (!path) {
        path = "/"; // conforms to RFC 2616 section 3.2.2
    }
    // we know that there is no query and no fragment here.
    return scheme + "://" + authority + path;
}

function normalizeParameters(parameters) {
  if (parameters == null) {
      return "";
  }
  var list = getParameterList(parameters);
  var sortable = [];
  for (var p = 0; p < list.length; ++p) {
      var nvp = list[p];
      if (nvp[0] != "oauth_signature") {
          sortable.push([ percentEncode(nvp[0])
                        + " " // because it comes before any character that can appear in a percentEncoded string.
                        + percentEncode(nvp[1])
                        , nvp]);
      }
  }
  sortable.sort(function(a,b) {
                    if (a[0] < b[0]) return  -1;
                    if (a[0] > b[0]) return 1;
                    return 0;
                });
  var sorted = [];
  for (var s = 0; s < sortable.length; ++s) {
      sorted.push(sortable[s][1]);
  }
  return formEncode(sorted);
}

function formEncode(parameters) {
    var form = "";
    var list = getParameterList(parameters);
    for (var p = 0; p < list.length; ++p) {
        var value = list[p][1];
        if (value == null) value = "";
        if (form != "") form += '&';
        form += percentEncode(list[p][0])
          +'='+ percentEncode(value);
    }
    return form;
}

function getBaseString(message) {
    var URL = message.action;
    var q = URL.indexOf('?');
    var parameters;
    if (q < 0) {
        parameters = message.parameters;
    } else {
        // Combine the URL query string with the other parameters:
        parameters = decodeForm(URL.substring(q + 1));
        var toAdd = getParameterList(message.parameters);
        for (var a = 0; a < toAdd.length; ++a) {
            parameters.push(toAdd[a]);
        }
    }
    return percentEncode(message.method.toUpperCase())
     +'&'+ percentEncode(normalizeUrl(URL))
     +'&'+ percentEncode(normalizeParameters(parameters));
}

function getHeaders(message) {
  var parameters = message.parameters;
  var method =message.method;
  var reconstructedUrl = message.action;
  var reconstructedOauthParameters = '';
  var obj = {};
  for (var i = 0 ; i < parameters.length; i++) {
    obj[parameters[i][0]] = parameters[i][1];
  }
  var oauth_custom_secrect = 'oZalC0qpsLgcL-pkm2jyrYF6uD6zvIGbkCa3yF19ke8';
  var oauthClient= new oauth.OAuth(null, null, obj['oauth_consumer_key'], oauth_custom_secrect, null, null, obj['oauth_signature_method']);
  reconstructedOauthParameters = oauthClient._normaliseRequestParams(obj);
  // console.log('=========================reconstructedOauthParameters', reconstructedOauthParameters);
  var token_secrect = 'h2XMOUGT2pkRoKSf-gsU9OejMRNPYZgASfSvr__pdjc';
  var signature = oauthClient._getSignature(method, reconstructedUrl, reconstructedOauthParameters , token_secrect);
  // console.log('============signature:',signature, "method:", method, "reconstructedUrl:", reconstructedUrl, "reconstructedOauthParameters:", reconstructedOauthParameters, "token_secrect:", token_secrect);
  message.parameters.push(['oauth_signature', signature]);
  return getAuthorizationHeader("OAuth", message.parameters)
}
console.log(getHeaders({
  method: 'POST',
  action: 'http://localhost:31337/api/user/Nick/feed',
  parameters: getParameters()
}))